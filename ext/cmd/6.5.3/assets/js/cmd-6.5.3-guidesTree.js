DocsApp.guidesTree = {
    "Guides": [
        {
            "slug": "whats_new_cmd65",
            "name": "What's New in Cmd 6.6",
            "navTreeName": "Guides",
            "text": "What's New in Cmd 6.6",
            "idx": 1,
            "id": "whats_new_cmd65",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/whats_new_cmd65.html"
        },
        {
            "slug": "whats_new_cmd",
            "name": "What's New in Cmd 6",
            "navTreeName": "Guides",
            "text": "What's New in Cmd 6",
            "idx": 2,
            "id": "whats_new_cmd",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/whats_new_cmd.html"
        },
        {
            "slug": "fashion",
            "name": "Welcome to Fashion",
            "navTreeName": "Guides",
            "text": "Welcome to Fashion",
            "idx": 3,
            "id": "fashion",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/fashion.html"
        },
        {
            "slug": "cmd_upgrade_guide",
            "name": "Cmd 6 Upgrade Guide",
            "navTreeName": "Guides",
            "text": "Cmd 6 Upgrade Guide",
            "idx": 4,
            "id": "cmd_upgrade_guide",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/cmd_upgrade_guide.html"
        },
        {
            "slug": "cmd_upgrade_five",
            "name": "Cmd 5 Upgrade Guide",
            "navTreeName": "Guides",
            "text": "Cmd 5 Upgrade Guide",
            "idx": 5,
            "id": "cmd_upgrade_five",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/cmd_upgrade_five.html"
        },
        {
            "slug": "intro_to_cmd",
            "name": "Introduction to Sencha Cmd",
            "navTreeName": "Guides",
            "text": "Introduction to Sencha Cmd",
            "idx": 6,
            "id": "intro_to_cmd",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/intro_to_cmd.html"
        },
        {
            "slug": "cmd_upgrade",
            "name": "Understanding App Upgrade",
            "navTreeName": "Guides",
            "text": "Understanding App Upgrade",
            "idx": 7,
            "id": "cmd_upgrade",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/cmd_upgrade.html"
        },
        {
            "slug": "cmd_compiler",
            "name": "Compiler-Friendly Guidelines",
            "navTreeName": "Guides",
            "text": "Compiler-Friendly Guidelines",
            "idx": 8,
            "id": "cmd_compiler",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/cmd_compiler.html"
        },
        {
            "slug": "cordova_phonegap",
            "name": "Cordova and PhoneGap Apps",
            "navTreeName": "Guides",
            "text": "Cordova and PhoneGap Apps",
            "idx": 9,
            "id": "cordova_phonegap",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/cordova_phonegap.html"
        },
        {
            "slug": "resource_management",
            "name": "Resource Management",
            "navTreeName": "Guides",
            "text": "Resource Management",
            "idx": 10,
            "id": "resource_management",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/resource_management.html"
        },
        {
            "slug": "workspaces",
            "name": "Workspaces in Sencha Cmd",
            "navTreeName": "Guides",
            "text": "Workspaces in Sencha Cmd",
            "idx": 11,
            "id": "workspaces",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/workspaces.html"
        },
        {
            "slug": "progressive_web_apps",
            "name": "Progressive Web Apps",
            "displayNew": true,
            "navTreeName": "Guides",
            "text": "Progressive Web Apps",
            "idx": 12,
            "id": "progressive_web_apps",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/progressive_web_apps.html"
        },
        {
            "slug": "microloader",
            "name": "The Microloader",
            "navTreeName": "Guides",
            "text": "The Microloader",
            "idx": 13,
            "id": "microloader",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/microloader.html"
        },
        {
            "slug": "compatibility_matrix",
            "name": "Cmd Compatibility Matrix",
            "navTreeName": "Guides",
            "text": "Cmd Compatibility Matrix",
            "idx": 14,
            "id": "compatibility_matrix",
            "leaf": true,
            "iconCls": "fa fa-file-text-o",
            "href": "guides/compatibility_matrix.html"
        },
        {
            "name": "Ext JS",
            "slug": "extjs",
            "children": [
                {
                    "name": "Using Cmd with Ext JS 6",
                    "slug": "cmd_app",
                    "navTreeName": "Guides",
                    "text": "Using Cmd with Ext JS 6",
                    "idx": 1,
                    "id": "extjs/cmd_app",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/extjs/cmd_app.html"
                }
            ],
            "navTreeName": "Guides",
            "text": "Ext JS",
            "idx": 15,
            "id": "extjs",
            "iconCls": "fa fa-folder-o"
        },
        {
            "name": "Touch",
            "slug": "touch",
            "children": [
                {
                    "name": "Using Cmd w/ Sencha Touch",
                    "slug": "cmd_app",
                    "navTreeName": "Guides",
                    "text": "Using Cmd w/ Sencha Touch",
                    "idx": 1,
                    "id": "touch/cmd_app",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/touch/cmd_app.html"
                }
            ],
            "navTreeName": "Guides",
            "text": "Touch",
            "idx": 16,
            "id": "touch",
            "iconCls": "fa fa-folder-o"
        },
        {
            "name": "Sencha Cmd Packages",
            "slug": "cmd_packages",
            "children": [
                {
                    "name": "Cmd Packages",
                    "slug": "cmd_packages",
                    "navTreeName": "Guides",
                    "text": "Cmd Packages",
                    "idx": 1,
                    "id": "cmd_packages/cmd_packages",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/cmd_packages/cmd_packages.html"
                },
                {
                    "name": "Creating Cmd Packages",
                    "slug": "cmd_creating_packages",
                    "navTreeName": "Guides",
                    "text": "Creating Cmd Packages",
                    "idx": 2,
                    "id": "cmd_packages/cmd_creating_packages",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/cmd_packages/cmd_creating_packages.html"
                }
            ],
            "navTreeName": "Guides",
            "text": "Sencha Cmd Packages",
            "idx": 17,
            "id": "cmd_packages",
            "iconCls": "fa fa-folder-o"
        },
        {
            "name": "Advanced Sencha Cmd",
            "slug": "advanced_cmd",
            "children": [
                {
                    "name": "Advanced Sencha Cmd",
                    "slug": "cmd_advanced",
                    "navTreeName": "Guides",
                    "text": "Advanced Sencha Cmd",
                    "idx": 1,
                    "id": "advanced_cmd/cmd_advanced",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/advanced_cmd/cmd_advanced.html"
                },
                {
                    "name": "Inside the App Build Process",
                    "slug": "cmd_build",
                    "navTreeName": "Guides",
                    "text": "Inside the App Build Process",
                    "idx": 2,
                    "id": "advanced_cmd/cmd_build",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/advanced_cmd/cmd_build.html"
                },
                {
                    "name": "Sencha Compiler Reference",
                    "slug": "cmd_compiler_reference",
                    "navTreeName": "Guides",
                    "text": "Sencha Compiler Reference",
                    "idx": 3,
                    "id": "advanced_cmd/cmd_compiler_reference",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/advanced_cmd/cmd_compiler_reference.html"
                },
                {
                    "name": "Ant Integration",
                    "slug": "cmd_ant",
                    "navTreeName": "Guides",
                    "text": "Ant Integration",
                    "idx": 4,
                    "id": "advanced_cmd/cmd_ant",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/advanced_cmd/cmd_ant.html"
                },
                {
                    "name": "Sencha Cmd Metadata",
                    "slug": "cmd_metadata",
                    "navTreeName": "Guides",
                    "text": "Sencha Cmd Metadata",
                    "idx": 5,
                    "id": "advanced_cmd/cmd_metadata",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/advanced_cmd/cmd_metadata.html"
                },
                {
                    "name": "Sencha Cmd Reference",
                    "slug": "cmd_reference",
                    "navTreeName": "Guides",
                    "text": "Sencha Cmd Reference",
                    "idx": 6,
                    "id": "advanced_cmd/cmd_reference",
                    "leaf": true,
                    "iconCls": "fa fa-file-text-o",
                    "href": "guides/advanced_cmd/cmd_reference.html"
                }
            ],
            "navTreeName": "Guides",
            "text": "Advanced Sencha Cmd",
            "idx": 18,
            "id": "advanced_cmd",
            "iconCls": "fa fa-folder-o"
        }
    ]
}