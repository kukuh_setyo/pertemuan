Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'npm', 'name', 'email', 'phone'
    ],

    data: { items: [
        { npm: '15351001', name: 'Irham', email: "irham@student.uir.ac.id", phone: "555-111-1111" },
        { npm: '15351001', name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "555-222-2222" },
        { npm: '15351001', name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333" },
        { npm: '15351001', name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
